        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
                <div>
                    <img src="<?= base_url('assets/img/htci.png') ?>" width="150px" height="110px" alt="">
                </div>
            </a>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
            <div class="sidebar-heading">
                Dashboard
            </div>

            <li class="nav-item">
                <a class="nav-link" href="dashboard">
                    <span>Home</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="artikel">
                    <span>Artikel</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="event">
                    <span>Event</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="galery">
                    <span>Galery Foto</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="klien">
                    <span>Klien Kami</span></a>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="sidebar-heading">
                Login
            </div>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span>Sign In</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span>Sign Up</span></a>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">



        </ul>