<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <p style="text-align:justify"><img style=" float: left; margin:0.1px 20px 50px" src="<?= base_url('assets/img/about.jpg') ?>" width="550px">
    <h2><b>Tentang HTCI</b></h2>
    <h3>
        Sekilas tentang kami.
    </h3>
    <a>
        Adalah wadah organisasi klub sepeda motor Honda Tiger di Indonesia. Organisasi Honda Tiger Club Indonesia (HTCI) dideklarasikan di Bandung pada tanggal 10 Oktober 2004 dengan dihadiri oleh sejumlah club dari seluruh tanah air, hingga saat ini Honda Tiger Club Indonesia (HTCI) terdiri atas Pengurus Pusat (PP), 20 Pengurus Daerah (Pengda) dan 252 klub yang tersebar di seluruh kawasan Indonesia. Honda Tiger Club Indonesia (HTCI) merupakan organisasi resmi yang berbadan hukum, memiliki AD dan ART organisasi yang jelas, dikukuhkan secara legal melalui Akta Notaris no. 57 oleh Notariat A. Natasukarya. Peran organisasional Honda Tiger Club Indonesia (HTCI) adalah sebagai pelaksana koordinasi dari aktivitas dan pertukaran informasi seluruh klub anggotanya dimana peran dasar dari Honda Tiger Club Indonesia (HTCI) adalah penguatan jaringan dan rasa persaudaraan, terutama diantara klub anggota. Kegiatan resmi tahunan yang menjadi agenda Pengurus Pusat (PP-HTCI) adalah Musyawarah Nasional (MUNAS HTCI), Musyawarah Kerja Nasional (MUKERNAS), Wing Day, dan Jambore Tiger Nasional (JAMNAS), serta kegiatan lain yang ditetapkan oleh Pengurus Pusat (PP-HTCI) yang bersifat kegiatan bakti sosial ataupun event yang berskala regional maupun nasional..</h3>

        </p>
        <br>
        <br>
        <div>
            <p>
            <h1 style="text-align: center;"><b>SEJARAH</b></h1>
            </p>
            <a>Pada tahun 1993, PT Astra Honda Motor (AHM) mengeluarkan varian baru sepeda motor bertipe sport dengan kapasitas mesin 200cc, yaitu Honda Tiger, atau sering disebut juga GL-200.</a>
            <div style="text-align: justify;">
                <a>
                    <b>A. Sekilas tentang Honda Tiger Club Indonesia</b><br>
                    Honda Tiger adalah sepeda motor bertipe sport yang beredar di wilayah Indonesia. Honda Tiger merupakan sepeda motor dengan kapasitas mesin tertinggi yang diproduksi oleh PT AHM. Honda Tiger diproduksi pertama kali oleh PT AHM pada tahun 1993, dan berakhir pada tahun 2014. PT. Astra Honda Motor (AHM) menghentikan produksi (discontinue) motor Honda Tiger per Maret 2014, namun produksi spare part Honda Tiger akan tetap ada sampai 7 tahun ke depan (tahun 2021). Meskipun begitu, tidak tertutup kemungkinan di masa depan akan ada penerus Honda Tiger, karena PT AHM menyadari keberadaan klub-klub motor Honda Tiger yang ikatan persaudaraanya sangat kuat di Indonesia.
                </a>
            </div>
            <div style="text-align: justify;">
                <a>
                    <b>B. PERIODE PRA PEMBENTUKAN HONDA TIGER CLUB INDONESIA</b><br>
                    Honda Tiger memperoleh sambutan yang positif dari pasar Indonesia, yang ditunjukkan oleh banyaknya pemilik dan pengguna Honda Tiger di berbagai kota di Indonesia. Para pemilik Honda Tiger di berbagai kota di Indonesia, kemudian membentuk klub/komunitas Honda Tiger. Pembentukan klub/komunitas Honda Tiger di berbagai kota di Indonesia, dapat dikelompokkan pada 2 (dua) periode utama, yaitu: (1) Periode Pra HTCI (1993-2004), dan (2) Periode Pasca HTCI (2004-sekarang). Pada periode tahun 1993-2003, telah terbentuk klub/komunitas Honda Tiger di berbagai kota besar di Indonesia, seperti di Bandung, Jakarta, Jogjakarta, Semarang, Surabaya, Denpasar, Lombok, Makasar, Gorontalo, Menado, Palu, Banjarmasin, Samarinda, Balikpapan, Pontianak, Medan, Pekanbaru, Padang, Bengkulu, Palembang, dan Lampung. Tonggak awal klub motor Honda Tiger di Indonesia adalah berdirinya Tiger Association Bandung (TAB) di Bandung pada 23 Mei 1994.<br>

                    Pada periode 1993-2004, terdapat sejumlah kegiatan yang mempertemukan berbagai klub/komunitas Honda Tiger dalam rangka mencapai 3 tujuan utama: (i) membentuk dan meningkatkan rasa persaudaraan dan solidaritas diantara sesama pemilik motor Honda Tiger, (ii) memberikan manfaat sosial yang positif dari keberadaan klub/komunitas motor Honda Tiger bagi masyarakat luas, dan (iii) meningkatkan kesadaran dan kedisiplinan para pemilik Honda Tiger terhadap keamanan dan keselamatan berkendara di jalan raya.
                </a>
            </div>
            <div style="text-align: justify;">
                <a>
                    <b>C. PERSIAPAN DAN DEKLARASI PEMBENTUKAN HONDA TIGER CLUB INDONESIA</b><br>
                    Proposal awal tentang pendirian wadah klub motor Honda Tiger berskala nasional (Indonesia) disusun oleh Teddy Supriadi, untuk memenuhi permintaan Indra Panca sebagai Koordinator Persiapan Pembentukan wadah tersebut. Selanjutnya, proposal awal yang disusun oleh Teddy Supriadi dibahas dalam kegiatan diskusi di sekretariat TAB, jalan Virgo Bandung, pada bulan September 2003, yang dihadiri oleh Teddy Supriadi, Indra Panca, Benny Adityo, Saeful Arifin, Rudi Karpidol, dan Erland Friorie. Setelah draft proposal itu tersusun sebagai hasil akhir diskusi, kemudian ditunjukkan oleh Indra Panca kepada Rio Harahap (JTC), yang memberikan tanggapan positif dan selanjutnya membicarakan proposal itu dalam lingkup Asosiasi Tiger Jakarta (ATJ), yang beranggotakan 5 klub motor Honda Tiger (JTC, JHTC, BTO, BMJ, MTC). Selanjutnya, Indra Panca, Teddy Supriadi dan Rio Harahap secara intensif membahas desain organisasi yang akan dibentuk, terkait dengan nama organisasi, struktur organisasi, dan AD/ART. Pembahasan intensif ini berlangsung dalam rentang waktu September 2003 sampai dengan Mei 2004. Dalam pertemuan-pertemuan tersebut disepakati nama organisasi yang menghimpun berbagai klub motor Honda Tiger di seluruh Indonesia itu adalah Honda Tiger Club Indonesia (HTCI). Nama organisasi HTCI, digunakan pertama kali pada acara Jambore Nasional HTCI di Pantai Slaki, Lampung (29-30 Mei 2004). Rencana awalnya, Deklarasi organisasi HTC I semula akan dilakukan di acara ini, namun batal dilaksanakan karena ada musibah wafatnya salah seorang peserta di lokasi acara. Setelah deklarasi organisasi HTCI gagal diselenggarakan di Lampung, 3 (tiga) orang perumus ini melakukan proses persiapan yang lebih intensif dan menyepakati bahwa deklarasi HTCI akan dilaksanakan pada acara 1 Dekade TAB, yang diselenggarakan pada 9-10 Oktober 2004. Dalam acara tersebut dilakukan 3 (tiga) agenda utama, yaitu: (i) Perayaan 1 Dekade TAB, (ii) Pembuatan rekor IBOR (Indonesian Book of Record) berupa parkir motor Tiger terbanyak di landasan parkir pesawat, dan (iii) Musyawarah Nasional (Munas) I dan Deklarasi HTCI. Salah satu pertimbangan Deklarasi HTCI dilakukan pada event 1 Dekade TAB adalah asumsi bahwa kegiatan TAB biasanya dihadiri oleh banyak klub-klub Tiger di Indonesia, sehingga dapat dianggap memenuhi prinsip keterwakilan (representativeness) yang memadai dalam proses pengambilan keputusan dan memiliki legitimasi yang kuat. Pelaksanaan Musyawarah Nasional I (Munas I) HTCI dilaksanakan di Wisma Muladi TNA AU Lanud Husein Sastranegara Bandung, pada hari Sabtu, 9 Oktober 2004, mulai jam 22.00 sampai dengan 01.00 (dinihari). Munas I tersebut dihadiri oleh 41 klub Tiger, yang berasal dari wilayah DKI Jakarta (7 klub), Banten (4 klub), Jawa Barat (15 klub), Jawa Tengah (9 klub), Jawa Timur (3 klub), Sumatera (1 klub), Sulawesi (1 klub), dan Nusa Tenggara Barat (1 klub). Dalam Munas I tersebut, komunitas HTML (Honda Tiger Mailing List) juga hadir mempresentasikan eksistensinya, baik di dunia maya maupun dunia nyata. Namun, HTML menyatakan diri tidak bergabung dengan organisasi HTCI.
                </a>
            </div>
        </div>




</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->