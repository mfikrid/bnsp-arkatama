<section class="akame-blog-details-area section-padding-80">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <div class="post-content">
                    <div class="post-meta">
                        <a href="#" class="post-date"><i class="icon_clock_alt"></i> March 04, 2022</a>
                        <a href="#" class="post-comments"><i class="icon_chat_alt"></i> 0</a>
                    </div>
                    <h2 class="post-title">Hajatan 4 Tahunan PENGDA HTCI JATIM Blakra'an lll</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="post-thumbnail mb-50">
                    <center><a class="thumbnail-zoom" href="https://tiger-club.or.id/upload/blogs/konten-hajatan-4-tahunan-pengda-htci-jatim-blakraan-lll.jpeg?time=1671596528"><img src="https://tiger-club.or.id/upload/blogs/thumbnail/konten-hajatan-4-tahunan-pengda-htci-jatim-blakraan-lll.jpeg?time=1671596528" alt="Gambar konten - Hajatan 4 Tahunan PENGDA HTCI JATIM Blakra'an lll"></a></center>
                </div>
            </div>
        </div>
        <br>
        <div class="row justify-content-center">
            <div class="col-2 col-md-2 col-lg-1">
                <!-- Post Share -->
                <div class="akame-post-share">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://tiger-club.or.id/blog/hajatan-4-tahunan-pengda-htci-jatim-blakraan-lll&amp;display=popup" target="_blank" data-toggle="tooltip" data-placement="left" title="" class="facebook" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/home?status=Hajatan 4 Tahunan PENGDA HTCI JATIM Blakra'an lll:%20https://tiger-club.or.id/blog/hajatan-4-tahunan-pengda-htci-jatim-blakraan-lll" target="_blank" data-toggle="tooltip" data-placement="left" title="" class="twitter" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="https://plus.google.com/share?url=https://tiger-club.or.id/blog/hajatan-4-tahunan-pengda-htci-jatim-blakraan-lll" target="_blank" data-toggle="tooltip" data-placement="left" title="" class="google-plus" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a>

                </div>
            </div>
            <br>

            <div class="col-10 col-md-10 col-lg-9">
                <div class="blog-details-text">
                    <p style="text-align: center;"><span>INFO PENGDA HTCI JAWA TIMUR</span></p>

                    <p>Support &amp; Ramaikan</p>

                    <p>Hajatan 4 Tahunan PENGDA HTCI JATIM Blakra'an lll,</p>

                    <p>Dengan tema : "Expedisi Jayabaya"</p>

                    <p>Waktu : Kediri 19 Maret 2022</p>

                    <p>Start : Jam 07.00 SIMPANG LIMA GUMUL</p>

                    <p>Finish : Air terjun Dolo.</p>

                    <p>Melewati berbagai macam destinasi wisata kota Kediri, tempat2 bersejarah, serta tempat2 kekinian kota kediri.</p>

                    <p>Lintasan sepanjang 65KM :<br>
                        40% jalan Aspal<br>
                        30% jalan Makadam<br>
                        30% jalan tanah</p>

                    <p>Persiapkan MacanMu</p>

                    <p>Biaaya pendaftaran 175.000K ( Jersy,pin,Makan+Snack)</p>

                    <p>Ojo lali Rek Gasssss......</p>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</section>