<section class="akame-blog-details-area section-padding-80">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <div class="post-content">
                    <div class="post-meta">
                        <a href="#" class="post-date"><i class="icon_clock_alt"></i> February 28, 2022</a>
                        <a href="#" class="post-comments"><i class="icon_chat_alt"></i> 0</a>
                    </div>
                    <h2 class="post-title">Kumpul Guyub seluruh club PengDa HTCI JaTeng-DIY</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="post-thumbnail mb-50">
                    <center><a class="thumbnail-zoom" href="https://tiger-club.or.id/upload/blogs/konten-kumpul-guyub-seluruh-club-pengda-htci-jateng-diy.jpeg?time=1671596863"><img src="https://tiger-club.or.id/upload/blogs/thumbnail/konten-kumpul-guyub-seluruh-club-pengda-htci-jateng-diy.jpeg?time=1671596863" alt="Gambar konten - Kumpul Guyub seluruh club PengDa HTCI JaTeng-DIY"></a></center>
                </div>
            </div>
        </div>
        <br>

        <div class="row justify-content-center">
            <div class="col-2 col-md-2 col-lg-1">
                <!-- Post Share -->
                <div class="akame-post-share">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://tiger-club.or.id/blog/kumpul-guyub-seluruh-club-pengda-htci-jateng-diy&amp;display=popup" target="_blank" data-toggle="tooltip" data-placement="left" title="" class="facebook" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/home?status=Kumpul Guyub seluruh club PengDa HTCI JaTeng-DIY:%20https://tiger-club.or.id/blog/kumpul-guyub-seluruh-club-pengda-htci-jateng-diy" target="_blank" data-toggle="tooltip" data-placement="left" title="" class="twitter" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="https://plus.google.com/share?url=https://tiger-club.or.id/blog/kumpul-guyub-seluruh-club-pengda-htci-jateng-diy" target="_blank" data-toggle="tooltip" data-placement="left" title="" class="google-plus" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a>

                </div>
            </div>
            <br>
            <br>
            <div class="col-10 col-md-10 col-lg-9">
                <div class="blog-details-text">
                    <p style="text-align: center;"><span>INFO PENGDA HTCI JATENG-DIY</span></p>

                    <p>Assalamu'alaikum Wr Wb</p>

                    <p>Salam Sejahtera</p>

                    <p>Salam satu sikap</p>

                    <p>Persembahan untuk seluruh keluarga besar Pengda HTCI Jateng-Diy.</p>

                    <p>KUMPUL GUYUB HTCI PENGDA JATENG-DIY</p>

                    <p>ORA ONO KOWE ORA RAME.</p>

                    <p>Hari/Tanggal : Sabtu 12 Maret 2022</p>

                    <p>Waktu : 19.00-22.00 WIB</p>

                    <p>Tempat : Wonosobo</p>

                    <p>Agenda : Kumpul Guyub seluruh club Pengda HTCI Jateng-Diy</p>

                    <p>CP ketua panitia : 081392382007 an Aziz Muslim</p>

                    <p>Agendakan, Ramaikan dan sukseskan acara untuk bertemu, berkumpul dan bersilaturahmi kembali dalam kebersamaan dan kehangatan Jateng-Diy.</p>

                    <p>Tak tunggu neng Wonosobo maseehh</p>

                    <p>Akan ada game interaktif dan doorprice !!...dan tentunya untuk mengabadikan moment ini.</p>

                    <p>Matursuwun</p>

                    <p>&nbsp;</p>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</section>