<section class="breadcrumb-area section-padding-80">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2>Berita Terbaru</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard"><i class="icon_house_alt"></i> Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Blog</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="akame-news-area section-padding-0-80">
    <div class="container">
        <div class="row">
            <!-- Single Blog Item -->
            <div class="col-sm-6 col-lg-4 px-4 blog-item" data-wow-delay="200ms" style="display: block;">
                <!-- Single Post Area -->
                <div class="single-post-area">
                    <div class="post-thumbnail">
                        <a><img style="margin: 0.4px 5px 0px" src="https://tiger-club.or.id/upload/blogs/thumbnail/konten-hajatan-4-tahunan-pengda-htci-jatim-blakraan-lll.jpeg?time=1671563241" alt="Hajatan 4 Tahunan PENGDA HTCI JATIM Blakra'an lll"></a>
                    </div>
                    <div class="post-content">
                        <a class="post-title" href="artikel/artikel1">Hajatan 4 Tahunan PENGDA HTCI JATIM Blakra'an lll</a>
                        <div class="post-meta">
                            <a class="post-date" href="artikel/artikel1"><i class=" icon_clock_alt"></i> March 04, 2022</a>
                            <a class="post-comments"><i class="icon_chat_alt"></i> 0</a>
                        </div>
                        <p>INFO PENGDA HTCI JAWA TIMUR

                            Support &amp;amp; Ramaikan

                            Hajatan 4 Tahunan PENGDA H...</p>
                    </div>
                </div>
            </div>
            <!-- Single Blog Item -->
            <div class="col-sm-6 col-lg-4 px-4 blog-item" data-wow-delay="200ms" style="display: block;">
                <div class="row">
                    <!-- Single Post Area -->
                    <div class="single-post-area">
                        <div class="post-thumbnail" style="margin: 0.4px 100px 0px">
                            <a><img src="https://tiger-club.or.id/upload/blogs/thumbnail/konten-kumpul-guyub-seluruh-club-pengda-htci-jateng-diy.jpeg?time=1671563241" alt="Kumpul Guyub seluruh club PengDa HTCI JaTeng-DIY"></a>
                            <a class="post-title" href="artikel/artikel2">Kumpul Guyub seluruh club PengDa HTCI JaTeng-DIY</a>
                            <div class="post-meta">
                                <a class="post-date" href="artikel/artikel2"><i class=" icon_clock_alt"></i> February 28, 2022</a>
                                <a class="post-comments"><i class="icon_chat_alt"></i> 0</a>
                            </div>
                            <p>INFO PENGDA HTCI JATENG-DIY

                                Assalamu&amp;#39;alaikum Wr Wb

                                Salam Sejahtera

                                ...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<br>
<br>
<br>
<br>