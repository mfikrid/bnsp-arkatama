<section class="section-padding-80-0">
    <div class="container">
        <div class="col-lg-12 table-responsive">
            <h3 align="center">Daftar Pengurus Pusat HTCI</h3>
            <br><br>
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/ketua-umum-caturmardiantost.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/ketua-umum-caturmardiantost.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Catur Mardianto ST
                        </th>
                        <th style="vertical-align:middle;">Ketua Umum</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/wakil-ketua-choirulanwar.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/wakil-ketua-choirulanwar.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Choirul Anwar
                        </th>
                        <th style="vertical-align:middle;">Wakil Ketua</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/sekretaris-jendral-rickyanwarifirdaus.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/sekretaris-jendral-rickyanwarifirdaus.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Ricky Anwari Firdaus
                        </th>
                        <th style="vertical-align:middle;">Sekretaris Jendral</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/keuangan-ismandanuwarsa.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/keuangan-ismandanuwarsa.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Isman Danu Warsa
                        </th>
                        <th style="vertical-align:middle;">Keuangan</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/kepala-bidang-admin-&amp;-jaringan-denihalawani.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/kepala-bidang-admin-&amp;-jaringan-denihalawani.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Deni Halawani
                        </th>
                        <th style="vertical-align:middle;">Kepala Bidang Admin &amp; Jaringan</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/tim-admin-&amp;-jaringan-devihandoyo.jpg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/tim-admin-&amp;-jaringan-devihandoyo.jpg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Devi Handoyo
                        </th>
                        <th style="vertical-align:middle;">Tim Admin &amp; Jaringan</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/kepala-bidang-pengawasan-mirwan.JPG?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/kepala-bidang-pengawasan-mirwan.JPG?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">M. Irwan
                        </th>
                        <th style="vertical-align:middle;">Kepala Bidang Pengawasan</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/tim-bidang-pengawasan-msphasibuan.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/tim-bidang-pengawasan-msphasibuan.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">M.S.P Hasibuan
                        </th>
                        <th style="vertical-align:middle;">Tim Bidang Pengawasan</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/kepala-bidang-program-kerja-rainulyakin.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/kepala-bidang-program-kerja-rainulyakin.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">R.Ainul Yakin
                        </th>
                        <th style="vertical-align:middle;">Kepala Bidang Program Kerja</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/tim-bidang-program-kerja-sehzaenudin.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/tim-bidang-program-kerja-sehzaenudin.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Seh Zaenudin
                        </th>
                        <th style="vertical-align:middle;">Tim Bidang Program Kerja</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/kepala-bidang-niaga-internal-syahdhanionangprastowo.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/kepala-bidang-niaga-internal-syahdhanionangprastowo.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Syahdhani Onang Prastowo
                        </th>
                        <th style="vertical-align:middle;">Kepala Bidang Niaga Internal</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/tim-niaga-internal-mharivictoria.JPG?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/tim-niaga-internal-mharivictoria.JPG?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">M. Hari Victoria
                        </th>
                        <th style="vertical-align:middle;">Tim Niaga Internal</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/kepala-bidang-niaga-eksternal-suprapto.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/kepala-bidang-niaga-eksternal-suprapto.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Suprapto
                        </th>
                        <th style="vertical-align:middle;">Kepala Bidang Niaga Eksternal</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/kepala-bidang-telematika-nickoanggoro.JPG?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/kepala-bidang-telematika-nickoanggoro.JPG?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Nicko Anggoro
                        </th>
                        <th style="vertical-align:middle;">Kepala Bidang Telematika</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/tim-telematika-yagussiswantoro.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/tim-telematika-yagussiswantoro.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Y Agus Siswantoro
                        </th>
                        <th style="vertical-align:middle;">Tim Telematika</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/kepala-bidang-humas-franzsergei.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/kepala-bidang-humas-franzsergei.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Franz Sergei
                        </th>
                        <th style="vertical-align:middle;">Kepala Bidang Humas</th>
                    </tr>
                    <tr>
                        <th width="20%"><a href="https://tiger-club.or.id/upload/pp-htci/kepala-bidang-sosial-andrikurniawan.jpeg?1671559756" class="thumbnail-zoom"><img width="115px" height="200px" class="img-responsive" src="https://tiger-club.or.id/upload/pp-htci/thumbnail/kepala-bidang-sosial-andrikurniawan.jpeg?1671559756" alt=""></a>
                        </th>
                        <th style="vertical-align:middle;">Andri Kurniawan
                        </th>
                        <th style="vertical-align:middle;">Kepala Bidang Sosial</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>