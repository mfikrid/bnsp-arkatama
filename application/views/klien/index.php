<div class="section">
    <div class="container">
        <h3 class="py-3">Klien Honda Tiger Club Indonesia</h3>
        <nav class="navbar navbar-expand-lg ">
            <div class="container">
                <div class="navbar-translate">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-danger" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse has-image" id="example-navbar-danger" style="background: url(&quot;https://muslimbiker.id/assets/img/blurred-image-1.jpg&quot;) 0% 0% / cover;">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">

                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="row" style="text-align: center;">
            <div class="col-lg-3 col-sm-6 my-3">
                <div class="card">
                    <img class="card-img-top" src="<?= base_url('assets/img/raffi.jpg') ?>">
                    <div class="card-body">
                        <h5 class="card-text mb-3">Raffi Ahmad</h5>
                        <p>Rans Entertainment</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 my-3">
                <div class="card">
                    <img class="card-img-top" src="<?= base_url('assets/img/baim.jpeg') ?>">
                    <div class="card-body">
                        <h5 class="card-text mb-3">Baim Wong</h5>
                        <p>Tiger Wong Entertainment </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 my-3">
                <div class="card">
                    <img class="card-img-top" src="<?= base_url('assets/img/atta.jpg') ?>">
                    <div class="card-body">
                        <h5 class="card-text mb-3">Atta Halilintar</h5>
                        <p>AH</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 my-3">
                <div class="card">
                    <img class="card-img-top" src="<?= base_url('assets/img/sule.jpeg') ?>">
                    <div class="card-body">
                        <h5 class="card-text mb-3">Sutisna</h5>
                        <p>Sule Production</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>