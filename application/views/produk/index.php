<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('cpanel/vendor/datatables/dataTables.bootstrap4.css') }}">
<!-- Content Header (Page header) -->
<div class="col-sm-12">

</div>
<!-- Main content -->
<section class="akame-service-area section-padding-80-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section Heading -->
                <div class="section-heading text-center">
                    <h2>Online Store</h2>
                    <p>Toko online Official Merchandise HTCI</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h4>Produk Kategori</h4>
                <div class="form-group">
                    <select class="form-control" id="filterKategori" name="filterKategori">
                        <option value="">Semua Kategori</option>
                        <option value="sticker">STICKER</option>
                        <option value="kaos-official-htci-2021">KAOS OFFICIAL HTCI</option>
                        <option value="sticker-cutting">STICKER CUTTING</option>
                        <option value="emblim-bordir">EMBLIM BORDIR</option>
                        <option value="gantungan-kunci">GANTUNGAN KUNCI</option>
                        <option value="kaos-hut">KAOS HUT</option>
                        <option value="topi">TOPI</option>
                    </select>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 product-item" style="display: block;">
                <div class="single-service-area mb-80 wow fadeInUp" style="min-height: 85%; visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;" data-wow-delay="200ms">
                    <a title="KAOS LENGAN PANJANG DEWASA">
                        <img src="https://tiger-club.or.id/upload/pos/products/kaos-lengan-panjang-dewasa/thumbnail/gambar-product-kaoslenganpanjangdewasa-1.jpg?time=1671558586" alt="Gambar Produk KAOS LENGAN PANJANG DEWASA">
                        <h5>KAOS LENGAN PANJANG DEWASA</h5>
                        <h6>KAOS OFFICIAL HTCI</h6>
                        <p>Bidang Bisnis HTCI</p>
                        <p><b>Rp. 115.000,00</b></p>
                    </a>
                    <hr>
                    <a class="btn btn-sm btn-success"><i class="fa fa-shopping-cart"></i> Add to Chart</a>

                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 product-item" style="display: block;">
                <div class="single-service-area mb-80 wow fadeInUp" style="min-height: 85%; visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;" data-wow-delay="200ms">
                    <a title="KAOS LENGAN PENDEK ANAK">
                        <img src="https://tiger-club.or.id/upload/pos/products/kaos-lengan-pendek-anak/thumbnail/gambar-product-kaoslenganpendekanak-1.jpg?time=1671558586" alt="Gambar Produk KAOS LENGAN PENDEK ANAK">
                        <h5>KAOS LENGAN PENDEK ANAK</h5>
                        <h6>KAOS OFFICIAL HTCI</h6>
                        <p>Bidang Bisnis HTCI</p>
                        <p><b>Rp. 75.000,00</b></p>
                    </a>
                    <hr>
                    <a class="btn btn-sm btn-success"><i class="fa fa-shopping-cart"></i> Add to Chart</a>

                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 product-item" style="display: block;">
                <div class="single-service-area mb-80 wow fadeInUp" style="min-height: 85%; visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;" data-wow-delay="200ms">
                    <a title="KAOS LENGAN PANJANG ANAK">
                        <img src="https://tiger-club.or.id/upload/pos/products/kaos-lengan-panjang-anak/thumbnail/gambar-product-kaoslenganpanjanganak-1.jpg?time=1671558586" alt="Gambar Produk KAOS LENGAN PANJANG ANAK">
                        <h5>KAOS LENGAN PANJANG ANAK</h5>
                        <h6>KAOS OFFICIAL HTCI</h6>
                        <p>Bidang Bisnis HTCI</p>
                        <p><b>Rp. 85.000,00</b></p>
                    </a>
                    <hr>
                    <a class="btn btn-sm btn-success"><i class="fa fa-shopping-cart"></i> Add to Chart</a>

                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 product-item" style="display: block;">
                <div class="single-service-area mb-80 wow fadeInUp" style="min-height: 85%; visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;" data-wow-delay="200ms">
                    <a title="KAOS LENGAN PENDEK DEWASA">
                        <img src="https://tiger-club.or.id/upload/pos/products/kaos-lengan-pendek-dewasa/thumbnail/gambar-product-kaoslenganpendekdewasa-1.jpg?time=1671558586" alt="Gambar Produk KAOS LENGAN PENDEK DEWASA">
                        <h5>KAOS LENGAN PENDEK DEWASA</h5>
                        <h6>KAOS OFFICIAL HTCI</h6>
                        <p>Bidang Bisnis HTCI</p>
                        <p><b>Rp. 100.000,00</b></p>
                    </a>
                    <hr>
                    <a class="btn btn-sm btn-success"><i class="fa fa-shopping-cart"></i> Add to Chart</a>

                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 product-item" style="display: block;">
                <div class="single-service-area mb-80 wow fadeInUp" style="min-height: 85%; visibility: hidden; animation-delay: 200ms; animation-name: none;" data-wow-delay="200ms">
                    <a title="Topi Snapback">
                        <img src="https://tiger-club.or.id/upload/pos/products/topi-snapback/thumbnail/gambar-product-topisnapback-4.jpg?time=1671558586" alt="Gambar Produk Topi Snapback">
                        <h5>Topi Snapback</h5>
                        <h6>Topi</h6>
                        <p>KELAPS</p>
                        <p><b>Rp. 115.000,00</b></p>
                    </a>
                    <hr>
                    <a class="btn btn-sm btn-success"><i class="fa fa-shopping-cart"></i> Add to Chart</a>

                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->
<!-- DataTables -->
<script src="{{ asset('cpanel/vendor/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('cpanel/vendor/datatables/dataTables.bootstrap4.js') }}"></script>
<!-- Page Script -->
<script>
    $(document).ready(function() {
        var table = $('#table-data').DataTable();

        $('.dataTables_filter input').unbind().bind('keyup', function() {
            var colIndex = document.querySelector('#table-data-filter-column').selectedIndex;
            table.column(colIndex).search(this.value).draw();
        });
    });
</script>