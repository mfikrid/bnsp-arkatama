<section class="akame-service-area section-padding-80-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section Heading -->
                <div class="section-heading text-center">
                    <h2>Jambore Nasional</h2>
                    <p>JAMNAS HTCI</p>
                </div>
            </div>
        </div>

        <div class="row">
            <p align="justify"><b>Jambore Nasional</b> atau disingkat dengan sebutan <b>JAMNAS</b> merupakan kegiatan resmi organisasi Honda Tiger Club Indonesia (HTCI) yang dilakukan setiap 2 (dua) tahun sekali, dengan agenda mengumpulkan semua anggota klub-klub yang tergabung di dalam organisasi Honda Tiger Club Indonesia (HTCI).
            </p>
            <ol>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional I HTCI Tahun 2005 Semarang, Jawa Tengah.</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional II HTCI Tahun 2006 Serang, Banten.</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional III HTCI Tahun 2007 Senggigi, Lombok. NTB</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional IV HTCI Tahun 2008 Padang, Sumbar</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional V HTCI Tahun 2010 Manado, Sulawesi Utara</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional VI HTCI Tahun 2013 Magelang, Jawa Tengah</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional VII HTCI Tahun 2015 Banyuwangi, Jawa Timur</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional VIII HTCI Tahun 2017 Sukabumi, Jawa Barat</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional IX HTCI Tahun 2019 Bandar Lampung, Lampung</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Jambore Nasional X HTCI Tahun 2021</li>
            </ol>
            <p></p>
        </div>
    </div>
</section>
<section class="akame-service-area section-padding-80-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section Heading -->
                <div class="section-heading text-center">
                    <h2>Musyawarah Nasional</h2>
                    <p>Munas HTCI</p>
                </div>
            </div>
        </div>

        <div class="row">
            <p align="justify">Musyawarah Nasional atau disingkat dengan sebutan Munas merupakan kegiatan resmi organisasi Honda Tiger Club Indonesia (HTCI) yang dilakukan setiap 2 tahun sekali dengan agenda pemilihan Ketum Umum Pengurus Pusat (PP) – HTCI. Adapun Munas yang pernah di selenggarakan adalah sebagai berikut :
            </p>
            <ol>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Munas I 2004 di Bandung, Jawa Barat</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Munas II 2005 di Semarang, Jawa Tengah</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Munas III 2007 di Semarang, Jawa Tengah</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Munas IV 2009 di Surabaya, Jawa Timur</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Munas V 2011 di Balikpapan, Kalimantan Timur</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Munas VI 2013 di Jakarta</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Munas VII 2015 di Bogor, Jawa Barat</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Munas VIII 2017 di Yogyakarta, Jawa Tengah</li>
                <li><i class="fa fa-circle" aria-hidden="true"></i> Munas IX 2019 di Semarang, Jawa Tengah</li>
            </ol>
            <p></p>
        </div>
    </div>
</section>