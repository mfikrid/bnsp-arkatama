<style>
    img.zoom {
        width: 350px;
        height: 200px;
        -webkit-transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        -ms-transition: all .2s ease-in-out;
    }

    .transisi {
        -webkit-transform: scale(1.8);
        -moz-transform: scale(1.8);
        -o-transform: scale(1.8);
        transform: scale(1.8);
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
    <div>
        <!-- Page Heading -->

        <div>
            <img src="<?= base_url('assets/img/galery.jpg') ?>" height="480px">
            <!-- /.card-body -->
        </div>
        <br>
        <div style="text-align: center;">
            <div>
                <img class="zoom" src="<?= base_url('assets/img/gal1.jpg') ?>" height="250px">
                <img class="zoom" src="<?= base_url('assets/img/gal2.jpeg') ?>" height="250px">
                <img class="zoom" src="<?= base_url('assets/img/gal3.jpg') ?>" height="250px">
            </div>
            <br>
            <div>
                <img class="zoom" src="<?= base_url('assets/img/gal1.jpg') ?>" height="250px">
                <img class="zoom" src="<?= base_url('assets/img/gal2.jpeg') ?>" height="250px">
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.container-fluid -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.zoom').hover(function() {
                $(this).addClass('transisi');
            }, function() {
                $(this).removeClass('transisi');
            });
        });
    </script>