<!-- Begin Page Content -->
<div class="container-fluid">
    <div>
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
        <div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7927.1424318102345!2d107.765113!3d-6.575672!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xeb0222ca5bcb96c8!2sHonda%20Tiger%20Club%20Indonesia%20(HTCI)!5e0!3m2!1sen!2sid!4v1671556607144!5m2!1sen!2sid" width="1260" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            <!-- /.card-body -->
        </div>
        <br>
        <ul class="navbar-nav ml-center">
            <section class="contact-information-area section-padding-80-0">
                <div class="container" style="text-align:center">
                    <div class="row">
                        <!-- Single Contact Information -->
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="single-contact-information mb-80">
                                <i class="fab fa-whatsapp"></i>
                                <h4>Whatsapp</h4>
                                <a href="https://Wa.me/+6282110102004">+62 821 1010 2004</a>
                            </div>
                        </div>

                        <!-- Single Contact Information -->
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="single-contact-information mb-80">
                                <i class="fa fa-home"></i>
                                <h4>Address</h4>
                                <a href="https://goo.gl/maps/ZS5eR1UWNVvqTsir7">Jl. A. Natasukarya No.30 Subang, Jawa Barat. 41214</a>
                            </div>
                        </div>

                        <!-- Single Contact Information -->
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="single-contact-information mb-80">
                                <i class="fa fa-clock"></i>
                                <h4>Open time</h4>
                                <a href="#">Everyday Open</a>
                            </div>
                        </div>

                        <!-- Single Contact Information -->
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="single-contact-information mb-80">
                                <i class="fa fa-envelope"></i>
                                <h4>Email</h4>
                                <a href="mailto:info@tiger-club.or.id">info@tiger-club.or.id</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </ul>
        <div class="social-links">
            <a href="https://www.facebook.com/adiityaanugrah/" target="_nextTab" class="outer-shadow hover-in-shadow link-card" data-tilt="" data-tilt-scale="1.1" style="transform: perspective(1000px) rotateX(0deg) rotateY(0deg) scale3d(1, 1, 1);"><i class="fab fa-facebook-f"></i></a>
            <a href="https://www.youtube.com/channel/UC1XwKHnv70Z5U6CZifTIbRw" target="_nextTab" class="outer-shadow hover-in-shadow link-card" data-tilt="" data-tilt-scale="1.1" style="transform: perspective(1000px) rotateX(0deg) rotateY(0deg) scale3d(1, 1, 1);"><i class="fab fa-youtube"></i></a>
            <a href="https://github.com/Aadityaanugrah10" target="_nextTab" class="outer-shadow hover-in-shadow link-card" data-tilt="" data-tilt-scale="1.1" style="transform: perspective(1000px) rotateX(0deg) rotateY(0deg) scale3d(1, 1, 1); will-change: transform;"><i class="fab fa-github"></i></a>
            <a href="https://www.instagram.com/adiityaanugrah" target="_nextTab" class="outer-shadow hover-in-shadow link-card" data-tilt="" data-tilt-scale="1.1" style="transform: perspective(1000px) rotateX(0deg) rotateY(0deg) scale3d(1, 1, 1); will-change: transform;"><i class="fab fa-instagram"></i></a>
            <a href="https://www.linkedin.com/in/aditya-anugrah/" target="_nextTab" class="outer-shadow hover-in-shadow link-card" data-tilt="" data-tilt-scale="1.1" style="transform: perspective(1000px) rotateX(0deg) rotateY(0deg) scale3d(1, 1, 1); will-change: transform;"><i class="fab fa-linkedin"></i></a>
                      
        </div>

        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.container-fluid -->